package ru.peo.mfiles;

import java.io.*;

/**
 * Класс программным способом создает файл целых чисел intdata.dat (путем подгружения чисел из текстового файлика input.txt
 */
public class IntData {
    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new FileReader("input.txt"));
        DataOutputStream output =  new DataOutputStream(new FileOutputStream("intdata.dat"));
        String number;
        try {
            while ((number = input.readLine()) != null) {//пока не равно нулю
                output.writeInt(Integer.parseInt(number));//writeInt - записываем целое число
                //Integer.parseInt(number) преобразовываем строку number в число
            }
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }finally {
            input.close();
            output.close();
        }
    }
}