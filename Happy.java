package ru.peo.mfiles;
import java.io.*;
/**
 * Класс из файла int6data.dat записывает в файл txt6data только "счастливые" числа.
 * Created by Елена
 */
public class Happy {
    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("int6data.dat"));
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("txt6data.txt"));
        try {
            while (dataInputStream.available() != 0) {// пока число непрочитанных байтов не равно нулю ..
                int nomber = dataInputStream.readInt();// считываем целое число из файлика int6data.dat
                if (Happy1(nomber))// если число является "счастливым" ...
                    dataOutputStream.writeChars(nomber + "\n");// записываем данные в файлик txt6data.dat
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            dataOutputStream.close();
            dataInputStream.close();
        }
    }
    //доступ к члену класса не предоставляется никому,
// кроме методов этого класса. Другие классы того же пакета также не могут обращаться к private-членам.
    //тип, используемый для представления логических величин.
    private static boolean Happy1(Integer num) {//метод,который делит на 10 наше число и записывает остаток в а(напр) напр 256/10=25(остаток)
        int a = num % 10;
        num = num / 10;
        int b = num % 10;
        num = num / 10;
        int c = num % 10;
        num = num / 10;
        int d = num % 10;
        num = num / 10;
        int e = num % 10;
        num = num / 10;
        int f = num % 10;
        num = num / 10;
        //остатки складываются
        int sum = a + b + c;
        int sum2 = d + e + f;
        return sum == sum2;
    }
}